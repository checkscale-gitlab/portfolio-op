<?php

namespace App\Controller;

use Knp\Snappy\Pdf;
use App\Entity\Formation;
use App\Entity\Experience;
use App\Repository\PostRepository;
use App\Repository\FicheRepository;
use App\Repository\FormationRepository;
use App\Repository\HardskillRepository;
use App\Repository\SoftskillRepository;
use App\Repository\ExperienceRepository;
use App\Repository\HobbiesRepository;
use App\Repository\ProjetsRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(
        PostRepository $postRepository,
        FicheRepository $ficheRepository,
        ExperienceRepository $experienceRepository,
        FormationRepository $formationRepository,
        SoftskillRepository $softskillRepository,
        HardskillRepository $hardskillRepository
    ): Response {
        $fiches = $ficheRepository->findBy([], ['id' => 'DESC'], 6);
        $posts = $postRepository->findBy([], ['id' => 'DESC'], 4);
        $experiences = $experienceRepository->findBy([], ['date' => 'DESC']);
        $formations = $formationRepository->findBy([], ['date' => 'DESC']);
        $softSkills = $softskillRepository->findAll();
        $hardSkills = $hardskillRepository->findAll();

        return $this->render('home/index.html.twig', [
            'posts' => $posts,
            'fiches' => $fiches,
            'experiences' => $experiences,
            'formations' => $formations,
            'softSkills' => $softSkills,
            'hardSkills' => $hardSkills,
            'current_menu' => 'home',
        ]);
    }

    /**
     * @Route("/generate-cv", name="generatecv")
     */
    public function cv(
        Pdf $snappy,
        FormationRepository $formationRepository,
        ExperienceRepository $experienceRepository,
        SoftskillRepository $softskillRepository,
        HardskillRepository $hardskillRepository,
        ProjetsRepository $projetsRepository,
        HobbiesRepository $hobbiesRepository,
        UserRepository $userRepository
    ) {
        $html = $this->renderView('home/cvpdf.html.twig', [
            'formations' => $formationRepository->findBy([], ['date' => 'DESC']),
            'experiences' => $experienceRepository->findBy([], ['date' => 'DESC']),
            'softs' => $softskillRepository->findBy([], ['titre' => 'ASC']),
            'hards' => $hardskillRepository->findBy([], ['titre' => 'ASC']),
            'projets' => $projetsRepository->findBy([], ['id' => 'ASC']),
            'hobbies' => $hobbiesRepository->findBy([], ['titre' => 'ASC']),
            'user' => $userRepository->findBy(['nom' => 'Prieur'], [])
        ]);

        //dd($userRepository->findBy(['nom' => 'Prieur'], []));

        $filename = 'Olivier-Prieur-CV-'.date('d-m-Y-H-i-s');

        $snappy->setTimeout(120);
        $snappy->setOption("enable-local-file-access",true);

        return new PdfResponse(
            $snappy->getOutputFromHtml($html),
            $filename.'.pdf',
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$filename.'.pdf"'
            ]
        );
    }
}
